import { GraphQLSchema } from "graphql";
import { makeExecutableSchema } from "graphql-tools";
import 'graphql-import-node';

import path from "path"
import { loadFilesSync } from "@graphql-tools/load-files";
import { mergeTypeDefs } from "@graphql-tools/merge";

import resolvers from './../resolvers/resolversMap';

const typesArray = loadFilesSync(path.join(__dirname, './graph'), { extensions: ['graphql'] })

const typeDefs = mergeTypeDefs(typesArray)



const schema: GraphQLSchema = makeExecutableSchema({
    typeDefs,
    resolvers
});

export default schema;