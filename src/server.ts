import express from 'express';
import compression from 'compression';
import cors from 'cors';
import schema from './schema';
import { ApolloServer } from 'apollo-server-express';
import { createServer } from 'http';
import env from './config/enviroment';
import DataBase from './config/db';
import expressPlayground from 'graphql-playground-middleware-express';

if(process.env.NODE_ENV !== 'production'){
    const envs = env
    console.log(env);
}

async function init(){
    const app = express();

    app.use( cors());
    
    app.use(compression());

    const database = new DataBase();
    const db = await  database.init()
    //Agrega el contexto el token
    const context: any = async({req, connection }:  any ) => {
        const token = req ? req.headers.authorization : connection.authorization;   
        return { db , token};
    }
    //Conexión al servidor del Apollo server
    const server = new ApolloServer({
        schema,
        context,
        introspection: true
    });
    
    server.applyMiddleware({ app });

    app.use('/', expressPlayground({
        endpoint: '/graphql'
    }));
    
    const PORT = process.env.PORT || 5300;
    const httpServer = createServer(app);
    httpServer.listen(
        { port : PORT },
        () => console.log(`API GraphQL... http://localhost:${PORT}/graphql`)
    );
}

init()
