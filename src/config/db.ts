// import MongoClient from 'mongodb';
import { MongoClient } from 'mongodb';
import * as mongoose from 'mongoose';
import config from '../env/index';
import chalk from 'chalk';

//Opciones
interface IConnectOptions {
    useNewUrlParser  : boolean;
    useUnifiedTopology: boolean
}

const connectOptions: IConnectOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true
};
//Conexión a la base de datos mongo
class DataBase { 

     async init(){
        const MONGO_URI: string = `${config.database.MONGODB_URI}${config.database.MONGODB_DB_MAIN}`;

        const client = await MongoClient.connect(MONGO_URI,  connectOptions);

        const db =  await client.db();
        
        if(client.isConnected ()) {
            console.log('------Db------');
            console.log(`Status ${chalk.greenBright('Online')}`);
            console.log(`${chalk.greenBright(db.databaseName)}`);
        }

        return db;
       
    }
 }

 export default DataBase;