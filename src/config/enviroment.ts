import dotenv from 'dotenv'
//Diferencia entre desarrollo y producción
const env = dotenv.config({path: './src/.env'});

if(process.env.NODE_ENV !== 'production'){
    if(env.error){
        throw env.error;
    }
}

export default env;