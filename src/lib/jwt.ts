import SECRET_KEY from "../config/constants";
import jwt from 'jsonwebtoken';

class JWT {
    private secretKey = SECRET_KEY as string;
    //Método para asignar y dar tienpoo de expiración al token
    sign(data:any){
        return jwt.sign({user: data.user}, this.secretKey, {expiresIn: 24 * 60 * 60});
    }
    //Método para verificar token
    verify(token:string){
        try{
            if ( token == "" && null) {
                return 'La auntenticación del token es invalida. Iniciua sesión para obtener un nuevo token';
            }
            
            return jwt.verify(token, this.secretKey) as string;
        }catch(error){
            return 'La auntenticación del token es invalida. Iniciua sesión para obtener un nuevo token';
        }
    }

    destroyToken(token: string){
        if(token){
            jwt.verify(token, this.secretKey) as string;

            token = '';

            return token
        }

    }
}

export default JWT;