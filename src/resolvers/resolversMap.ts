import { IResolvers } from "graphql-tools";
import mutation from "./mutations";
import query from "./query";
//Solucionadores de consulta
const resolvers : IResolvers = {
    ...query,
    ...mutation
}

export default resolvers;