import { IResolvers } from "graphql-tools";
import JWT from '../lib/jwt';
import  bcryptjs from 'bcryptjs';
import { ObjectId } from "mongodb";
     //COnsultas
const query : IResolvers = {
    Query : {
        //Método para obtener todos los usuarios
        async users(_: void, __:any, { db, token }): Promise<any>{
            // validación de token
            let info:any = new JWT().verify(token);
            
            if(info.code == 1) {
                return {
                    code: 'ERR',
                    message: info.msg,
                    user: null
                }
            }

            return {
                code:'OK',
                message: 'Se obtivieron los usuarioa',
                user: await db.collection('users').find().toArray()
            }
        },
        //método para logeo
        async login(_: void, { email, password }, { db }):Promise <any>{
            
            const user = await db.collection('users').findOne({email: email});
            

            if(user == null){
                return {
                    status: false,
                    message: 'Login incorrecto,  no existe el usuario',
                    token: null
                }
            }
            if( !bcryptjs.compareSync(password, user.password)){
                return {
                    status: false,
                    message: 'Login incorrecto, contraseña incorrecta',
                    token:null
                }
            }
            return {
                status: true,
                message:  'Login correcto',
                token:  new JWT().sign({ user }),
                user: user
            }
        },

        async logOut(_:void, {idUser}, { db, token }) {

            let info = new JWT().verify(token);

            if ( info == 'La auntenticación del token es invalida. Iniciua sesión para obtener un nuevo token' ) {
                return {
                    code: 'Err',
                    message: info,
                }
            }
            
            const user = await db.collection('users').findOne({'_id': new ObjectId(idUser)});

            if (user == null) {
                return {
                    code: 'err',
                    message: 'No existe el usuario',
                    user: null
                }
            }
            token = new JWT().destroyToken(token);
            return {
                code:'Ok',
                message: "Logout successfull",
                user: user             
            };
        },
        //Método para autenticar usuario
        auth(_:void, __:void, { token}) {
            let info:any = new JWT().verify(token);
            if(info === 'La auntenticación del token es invalida. Iniciua sesión para obtener un nuevo token'){
                return {
                    status: false,
                    message: info,
                    user: null
                }
            }
            return {
                status: true,
                message: 'token correcto ',
                user: info.user
            }
        }
    }
}

export default query;