import { IResolvers } from "graphql-tools";
import { Datetime } from '../lib/datetime';
import  bcryptjs from 'bcryptjs';
import JWT from '../lib/jwt';
import { ObjectId } from "mongodb";

const mutation : IResolvers = {
    Mutation : {
        //Método para registrar usuarios
        async register(_: void, { user }, { db } ): Promise<any> {

            const userCheck = await db.collection('users').findOne({email: user.email});

            if(userCheck !== null){
                return {
                    code: "Err",
                    message:`El usuario ya existe  ${user.name}`,
                    user: null
                }
            }

            const lastUser = await db.collection('users').find().limit(1).sort({registerDate: -1}).toArray();

            if(lastUser.length === 0) {
                user.id = 1 
            }else{
                user.id = lastUser[0].id + 1
            }

            //user.registerDate = new Datetime().getCurrentDateTime();
            user.password = bcryptjs.hashSync(user.password, 10)

            return await db.collection('users').insertOne(user).then((result: any) => {

                return {
                    code: "Ok",
                    message:`Usuario ${user.name} ${user.lastname} se añadió correctamente`,
                    user
                }
            }).catch((error:any) => {
                return {
                    code: 'Err',
                    message:`Usuario no se añadió: ${error}`,
                    user: null
                }
            });
        },

        async updateUser(_: void, { user, idUser }, { db, token}) {

            let info: any = new JWT().verify(token);

            if ( info.code == 1) {
                return {
                    code: 'err',
                    message: info.msg,
                }
            }

            const findUser = await db.collection('users').findOne({'_id': new ObjectId(idUser)});

            if( findUser == null) {
                return {
                    code: 'Err',
                    message: 'EL usuario no existe'
                }
            }

            const userUpdate = await db.collection('users').updateOne({'_id': new ObjectId(idUser)},{
                $set: {
                    name:     user.name ? user.name : findUser.name,
                    lastname: user.lastname ? user.lastname : findUser.lastname,
                    email:    user.email ? user.email  : findUser.email
                }
            });

            if(userUpdate.matchedCount === 1){
                //se busca usuario para retornar lo actualizado
                const userUpdated = await db.collection('users').findOne({'_id': new ObjectId(idUser)});
                return {
                    code: '2B-OK',
                    message: 'Users updated successfull',
                    user: userUpdated
                }
            } 


        },

        async deleteUser(_: void, { idUser }, { db, token }) {
            
            let info: any = new JWT().verify(token);

            if ( info.code == 1) {
                return {
                    code: 'err',
                    message: info.msg,
                }
            }

            // se busca el usuario
            const user = await db.collection('users').findOne({'_id': new ObjectId(idUser)});
            // se valida usuario
            if(user == null){
                return {
                    code: '2B-ERR',
                    message: 'El usuario no existe o ya fue eliminado de la base de datos',
                    user: null
                }
            }

            const userDeleted = await db.collection('users').deleteOne({'_id': new ObjectId(idUser)});
            
            if(userDeleted.deletedCount == 1){

                return {
                    code: '2B-OK',
                    message: `El usuario fue eliminado`,
                    user:  null
                }
            }

        }
    }
}

export default mutation;