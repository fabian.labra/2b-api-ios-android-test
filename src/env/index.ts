import * as dotenv from 'dotenv';

dotenv.config();

interface IConfig {
    port: string | number;
    database: {
        MONGODB_URI: string;
        MONGODB_DB_MAIN: string;
    };
}

const NODE_ENV: string =  'development';

const development: IConfig = {
    port: process.env.PORT || 5002,
    database: {
        MONGODB_URI: 'mongodb+srv://iosAndroid:3535@pruebas.b86oi.mongodb.net/Pruebas?retryWrites=true',
        MONGODB_DB_MAIN:'POC graphql'
    },
};

const config: {
    [name: string]: IConfig
} = {
    development,
};


export default config[NODE_ENV];
